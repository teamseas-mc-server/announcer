package org.teamseas.minecraft;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import java.util.Iterator;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.minimessage.MiniMessage;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The main class of the plugin.
 */
public class Plugin extends JavaPlugin {

  @Override
  public void onEnable() {
    saveDefaultConfig();

    FileConfiguration config = getConfig();
    ConfigurationSection rawAnnouncements = config.getConfigurationSection("announcements");
    
    if (rawAnnouncements == null) {
      getLogger().severe("No announcements found in config.yml");
      return;
    }

    Iterator<Component> announcements = Iterables.cycle(announcements(rawAnnouncements)).iterator();
    Runnable task = () -> audience().sendMessage(announcements.next());
    long interval = config.getLong("interval") * 20;

    getServer().getScheduler().runTaskTimer(this, task, interval, interval);
  }

  private static Audience audience() {
    return Bukkit.getOnlinePlayers().stream()
        .filter(player -> !player.hasPermission("teamseas.announcer.bypass"))
        .collect(Audience.toAudience());
  }

  private static ImmutableList<Component> announcements(ConfigurationSection section) {
    return section.getValues(true).values().stream()
        .map(String::valueOf)
        .map(String::trim)
        .map(MiniMessage.get()::parse)
        .collect(ImmutableList.toImmutableList());
  }

  @Override
  public void onDisable() {
    // TODO: remove this
  }
}
